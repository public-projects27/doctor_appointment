import 'dart:io';

import 'package:doctor_appointment/pages/home_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'components/drawer.dart';

class MainHomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MainHomePageState();
}

class MainHomePageState extends State<MainHomePage> {
  @override
  void initState() {
    super.initState();
  }

  SliverAppBar homeAppBar = new SliverAppBar(
    shadowColor: Colors.transparent,
      title: new Text("HiDoc."),
      pinned: true,
      elevation: 0,
      actions: <Widget>[
        new SizedBox(
          width: 10,
        ),
        new GestureDetector(
          child: new Icon(Icons.shopping_cart),
          onTap: () {},
        ),
        new SizedBox(
          width: 20,
        ),
        new GestureDetector(
          child: new Icon(Icons.notifications),
          onTap: () {},
        ),
        new Padding(
          padding: const EdgeInsets.symmetric(horizontal: 7),
        ),
      ]);

  Future<bool> _onWillPop() {
    return showDialog(
            context: context,
            builder: (context) {
              return new AlertDialog(
                    title: new Text(
                      "Do You Want Close HiDoc. ?",
                      style:
                          TextStyle(fontSize: 15, fontWeight: FontWeight.w400 ),
                    ),
                    content: new Text(
                      'Are You Sure?',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    ),
                    actions: <Widget>[
                      new FlatButton(
                          onPressed: () => Navigator.of(context).pop(false),
                          child: new Text(
                            'NO, I Stay Yet',
                            style: TextStyle(color: Colors.green),
                          )),
                      new FlatButton(
                          onPressed: () => exit(0),
                          child: new Text(
                            'YES',
                            style: TextStyle(color: Colors.red),
                          ))
                    ],
                  );
            }) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return new WillPopScope(
        child: new Scaffold(
            drawer: buildDrawerLayout(context),
            body: new NestedScrollView(
                headerSliverBuilder:
                    (BuildContext context, bool innerBoxIsScrolled) {
                  return <Widget>[homeAppBar];
                },
                body: new HomePage())),
        onWillPop: _onWillPop);
  }
}
