

import 'package:flutter/material.dart';

class TimeWorking {
  final Map<String, bool> workingDay;
  final int workingTimeStart;
  final int workingTimeEnd;

  TimeWorking({
    @required this.workingDay,
    @required this.workingTimeStart,
    @required this.workingTimeEnd,});

  String getWorkingDay(){
    String _workingDay = '';
    if(this.workingDay['saturday'])
      _workingDay = _workingDay + 'Sat';
    if(this.workingDay['sunday'])
      _workingDay = _workingDay + 'Sun';
    if(this.workingDay['monday'])
      _workingDay = _workingDay + 'Mon';
    if(this.workingDay['tuesday'])
      _workingDay = _workingDay + 'Tue';
    if(this.workingDay['wednesday'])
      _workingDay = _workingDay + 'Wed';
    if(this.workingDay['thursday'])
      _workingDay = _workingDay + 'Thu';
    if(this.workingDay['friday'])
      _workingDay = _workingDay + 'Fri';
    String _workingDayFirst = _workingDay.substring(0,3);
    String _workingDayLast = _workingDay.substring((_workingDay.length-3), (_workingDay.length));
    print('$_workingDayFirst - $_workingDayLast');
    return '$_workingDayFirst - $_workingDayLast';
  }

  String getWorkingTime(){
    String _workingTimeStart = '';
    String _workingTimeEnd = '';
    this.workingTimeStart <= 1300
        ? this.workingTimeStart.toString().length < 4
            ? _workingTimeStart = ('0' + this.workingTimeStart.toString().substring(0,1) + ':'  + this.workingTimeStart.toString().substring(1,3)  + ' AM' )
            : _workingTimeStart = ( this.workingTimeStart.toString().substring(0,2) + ':'  + this.workingTimeStart.toString().substring(2,4)  + ' AM' )
        : (this.workingTimeStart-1200).toString().length < 4
            ? _workingTimeStart = ('0' + (this.workingTimeStart - 1200).toString().substring(0,1) + ':'  + (this.workingTimeStart - 1200).toString().substring(1,3)  + ' PM' )
            : _workingTimeStart = ( (this.workingTimeStart - 1200).toString().substring(0,2) + ':'  + this.workingTimeStart.toString().substring(2,4)  + ' PM' )
    ;

    this.workingTimeEnd <= 1300
        ? this.workingTimeEnd.toString().length < 4
            ? _workingTimeEnd = ('0' + this.workingTimeEnd.toString().substring(0,1) + ':'  + this.workingTimeEnd.toString().substring(1,3)  + ' AM' )
            : _workingTimeEnd = ( this.workingTimeEnd.toString().substring(0,2) + ':'  + this.workingTimeEnd.toString().substring(2,4)  + ' AM' )
        : (this.workingTimeEnd-1200).toString().length < 4
            ? _workingTimeEnd = ('0' + (this.workingTimeEnd - 1200).toString().substring(0,1) + ':'  + (this.workingTimeEnd - 1200).toString().substring(1,3)  + ' PM' )
            : _workingTimeEnd = ( (this.workingTimeEnd - 1200).toString().substring(0,2) + ':'  + this.workingTimeEnd.toString().substring(2,4)  + ' PM' )
    ;

    print('$_workingTimeStart - $_workingTimeEnd');
        return '$_workingTimeStart - $_workingTimeEnd';
  }



}