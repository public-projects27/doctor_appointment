
import 'package:flutter/material.dart';

class Doctor {
  final String firstName;
  final String lastName;
  final String phoneNumber;
  final String email;
  final double rate;
  final int reviewsRate;
  final double price;
  final String special;
  final String degree;
  final String about;
  final int workingTimeStart;
  final int workingTimeEnd;
  final int experience;
  final Image image;
  final Image avatar;
  final Image cover;
  final Map<String, bool> workingDay;

  Doctor({
    @required this.firstName,
    @required this.lastName,
    @required this.phoneNumber,
    @required this.email,
    this.rate,
    @required this.reviewsRate,
    this.price,
    @required this.special,
    this.about,
    this.workingDay,
    this.workingTimeStart,
    this.workingTimeEnd,
    @required this.experience,
    this.image,
    this.avatar,
    this.cover,
    this.degree = ''
  });


}
