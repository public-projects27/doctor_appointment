import 'package:doctor_appointment/models/doctor.dart';
import 'package:doctor_appointment/models/money.dart';
import 'package:doctor_appointment/models/time_working.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class DoctorScreen extends StatefulWidget {
  final Doctor doctor;

  DoctorScreen({@required this.doctor});

  @override
  State<StatefulWidget> createState() => DoctorScreenState(doctor: doctor);
}

class DoctorScreenState extends State<DoctorScreen> {
  final Doctor doctor;
  double _price;

  DoctorScreenState({@required this.doctor});

  final List<String> _dropDownItems = <String>['   \$', '   ₹', 'ريال'];
  List<double> _priceItems;
  String _dropDownValue;

  @override
  void initState() {
    _dropDownValue = _dropDownItems[2];
    _price = doctor.price;
    _priceItems = <double>[
      Money(iranRial: _price).getDollar(),
      Money(iranRial: _price).getRupee(),
      _price,
    ];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var _screenSize = MediaQuery.of(context).size;

    AppBar doctorAppBar = new AppBar(
      automaticallyImplyLeading: false,
      toolbarHeight: 0,
    );
    Container imageCover = new Container(
      height: _screenSize.height * 0.43,
      color: Color(0xff0148fa),
      child: new Image(
        image: doctor.cover.image,
        fit: BoxFit.cover,
      ),
    );
    Container backButton = new Container(
        margin: const EdgeInsets.only(left: 15, top: 10),
        alignment: Alignment.topLeft,
        child: new GestureDetector(
          child: new Icon(Icons.arrow_back_ios),
          onTap: () {
            Navigator.pop(context);
          },
        ));

    Container information = new Container(
      height: _screenSize.height * 0.9,
        alignment: Alignment.topCenter,
      child: new Stack(
        children: [
          new Positioned(
              child: new DraggableScrollableSheet(
                  initialChildSize: 0.65,
                  minChildSize: 0.65,
                  maxChildSize: 0.85,
                  builder: (context, _scrollController) => Container(
                      child: new SingleChildScrollView(
                          controller: _scrollController,
                          child: new Container(
                              height: _screenSize.height * 0.8,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(35),
                                      topRight: Radius.circular(35))),
                              child: new Container(
                                margin: const EdgeInsets.symmetric(horizontal: 20),
                                child: new Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    new SizedBox(
                                      height: 25,
                                    ),
                                    new Row(
                                        mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                        children: [
                                          new Text(
                                            'Dr. ${doctor.firstName} ${doctor.lastName}',
                                            style: TextStyle(
                                                fontSize: 22,
                                                fontWeight: FontWeight.w800),
                                          ),
                                          new Row(
                                            children: [
                                              new Directionality(
                                                textDirection: TextDirection.rtl,
                                                child: new DropdownButton(
                                                    value: _dropDownValue,
                                                    dropdownColor:
                                                    Color(0xff5189f9),
                                                    icon: Icon(
                                                      Icons.keyboard_arrow_down,
                                                      color: Colors.black,
                                                    ),
                                                    iconSize: 20,
                                                    items: _dropDownItems.map<
                                                        DropdownMenuItem<
                                                            String>>(
                                                            (String value) {
                                                          return DropdownMenuItem<
                                                              String>(
                                                            value: value,
                                                            child: Text(value,
                                                                style: TextStyle(
                                                                    color:
                                                                    Colors.black)),
                                                          );
                                                        }).toList(),
                                                    onChanged: (String newValue) {
                                                      setState(() {
                                                        _dropDownValue = newValue;
                                                        if (newValue ==
                                                            _dropDownItems[0])
                                                          _price = _priceItems[0];
                                                        else if (newValue ==
                                                            _dropDownItems[1])
                                                          _price = _priceItems[1];
                                                        else if (newValue ==
                                                            _dropDownItems[2])
                                                          _price = _priceItems[2];
                                                      });
                                                    }),
                                              ),
                                              new SizedBox(
                                                width: 5,
                                              ),
                                              new SizedBox(
                                                width: 80,
                                                child: new Text('$_price'),
                                              )
                                            ],
                                          ),
                                        ]),
                                    new Text(
                                      '${doctor.special}',
                                      style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.grey),
                                    ),
                                    new SizedBox(
                                      height: 10,
                                    ),
                                    new Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      children: [
                                        new Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                          children: [
                                            new Row(children: [
                                              new SmoothStarRating(
                                                  allowHalfRating: false,
                                                  onRated: (v) {},
                                                  starCount: 5,
                                                  rating: doctor.rate,
                                                  size: 15.0,
                                                  isReadOnly: true,
                                                  filledIconData: Icons.star,
                                                  halfFilledIconData:
                                                  Icons.star_half,
                                                  color: Colors.amber,
                                                  borderColor: Colors.amber,
                                                  spacing: 0.0),
                                              new SizedBox(
                                                width: 10,
                                              ),
                                              new Text(
                                                doctor.rate.toString(),
                                                style:
                                                TextStyle(color: Colors.amber),
                                              ),
                                              new SizedBox(
                                                width: 15,
                                              ),
                                              new Text(
                                                '(${doctor.reviewsRate}+ Reviews)',
                                                style: TextStyle(
                                                    color: Colors.grey,
                                                    fontWeight: FontWeight.w300),
                                              )
                                            ]),
                                            new SizedBox(
                                              height: 30,
                                            ),
                                            new Row(
                                              mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                              children: [
                                                new Container(
                                                  height: 40,
                                                  width: 40,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                      BorderRadius.circular(12),
                                                      color: Color(0xffc0f0ff)),
                                                  child: new Icon(
                                                    Icons.call,
                                                    color: Colors.blueAccent,
                                                  ),
                                                ),
                                                new SizedBox(
                                                  width: 25,
                                                ),
                                                new Container(
                                                  height: 40,
                                                  width: 40,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                      BorderRadius.circular(12),
                                                      color: Color(0xffffd0e0)),
                                                  child: new Icon(
                                                    Icons.email,
                                                    color: Colors.redAccent,
                                                  ),
                                                ),
                                                new SizedBox(
                                                  width: 25,
                                                ),
                                                new Container(
                                                  height: 40,
                                                  width: 40,
                                                  decoration: BoxDecoration(
                                                      borderRadius:
                                                      BorderRadius.circular(12),
                                                      color: Color(0xffc0ffd0)),
                                                  child: new Icon(
                                                    Icons.videocam,
                                                    color: Colors.green,
                                                  ),
                                                ),
                                                new SizedBox(
                                                  width: 25,
                                                ),
                                              ],
                                            )
                                          ],
                                        ),
                                        new Container(
                                          height: 90,
                                          width: 60,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                              BorderRadius.circular(12),
                                              color: Color(0xffecf0fc)),
                                          child: new Column(
                                            mainAxisAlignment:
                                            MainAxisAlignment.center,
                                            children: [
                                              new Text(
                                                '${doctor.experience} +',
                                                style: TextStyle(
                                                    color: Colors.blueAccent,
                                                    fontSize: 17,
                                                    fontWeight: FontWeight.w700),
                                              ),
                                              new Text('Years',
                                                  style: TextStyle(
                                                      color: Colors.grey,
                                                      fontWeight: FontWeight.w400)),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                    new SizedBox(
                                      height: 30,
                                    ),
                                    new Text(
                                      'About',
                                      style: TextStyle(fontSize: 18),
                                    ),
                                    new SizedBox(
                                      height: 7,
                                    ),
                                    new Text(
                                      'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna ailqua. Ut enim ad minim veniam, quis nostrud exercitation.',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.grey,
                                          letterSpacing: 0.8),
                                    ),
                                    new SizedBox(
                                      height: 30,
                                    ),
                                    new Text(
                                      'Working Time',
                                      style: TextStyle(fontSize: 18),
                                    ),
                                    new SizedBox(
                                      height: 7,
                                    ),
                                    new Text(
                                      '${TimeWorking(workingDay: doctor.workingDay, workingTimeStart: doctor.workingTimeStart, workingTimeEnd: doctor.workingTimeEnd).getWorkingDay()}, ${TimeWorking(workingDay: doctor.workingDay, workingTimeStart: doctor.workingTimeStart, workingTimeEnd: doctor.workingTimeEnd).getWorkingTime()}',
                                      style: TextStyle(
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.grey),
                                    ),
                                    new Row(
                                      mainAxisAlignment: MainAxisAlignment.center,
                                      children: [
                                        new Container(
                                            margin: const EdgeInsets.only(top: 30),
                                            width: _screenSize.width * 0.7,
                                            height: 55,
                                            alignment: Alignment.center,
                                            decoration: BoxDecoration(
                                                color: Color(0xff0148fa),
                                                borderRadius: new BorderRadius.all(
                                                    const Radius.circular(30))),
                                            child: new Text(
                                              "Proceed",
                                              style: new TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 18,
                                                  fontWeight: FontWeight.w400,
                                                  letterSpacing: 0.8),
                                            )),
                                        new SizedBox(
                                          height: 20,
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              )))))),
        ],
      )

    );

    return new Scaffold(
        appBar: doctorAppBar,
        body: new Container(
          alignment: Alignment.topCenter,
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              new Stack(
                alignment: Alignment.topCenter,
                children: [imageCover, backButton, information],
              ),
            ],
          ),
        ));
  }
}
