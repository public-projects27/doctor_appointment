import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:table_calendar/table_calendar.dart';

class PickDate extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => PickDateState();
}

class PickDateState extends State<PickDate>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  CalendarController _calenderController;
  List<String> _locationPart1 = <String> [
    'Tehran',
    'Mississippi',
    'Kalyan Nagar',
  ];
  List<String> _locationPart2 = <String> [
    'Kasra General Hospital',
    'Merit Health Central',
    'North Bangalore Hospital',
  ];
  List<String> _morningHours = <String> [
    '08:00-08:30 AM',
    '08:30-09:00 AM',
    '09:30-10:00 AM',
    '10:00-10:30 AM',
    '10:30-11:00 AM',
    '11:30-12:00 PM',
  ];
  List<String> _afternoonHours = <String>[
    '12:00-12:30 PM',
    '12:30-01:00 PM',
    '01:30-02:00 PM',
    '02:00-02:30 PM',
    '02:30-03:00 PM',
    '03:30-04:00 PM',
  ];
  List<String> _eveningHours = <String> [
    '04:00-04:30 PM',
    '04:30-05:00 PM',
    '05:30-06:00 PM',
    '06:00-06:30 PM',
    '06:30-07:00 PM',
    '07:30-08:00 PM',
  ];


  // sat: 0
  // mon: 1
  // tue: 2
  // wed: 3
  // thu: 4
  // fri: 5
  // sun: 6
  DateTime _today = DateTime.now();
  DateTime _selectedDay;
  String _selectSlot;
  int _selectedTime;
  BoxDecoration _unSelectedDec = new BoxDecoration(
    border: Border.all(width: 1, color: Colors.grey),
    borderRadius: new BorderRadius.all(const Radius.circular(30)),
  );
  BoxDecoration _selectedDec = new BoxDecoration(
    color: Color(0xff0148fa),
    borderRadius: new BorderRadius.all(const Radius.circular(30)),
  );
  TextStyle _unselectedStyle =
  TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: Colors.grey);
  TextStyle _ableUnselectedStyle = TextStyle(fontSize: 12, fontWeight: FontWeight.w500, color: Colors.black);
  TextStyle _selectedStyle = TextStyle(fontWeight: FontWeight.w800, fontSize: 12, color: Colors.white);
  Color _selectedSlotColor;

  @override
  void initState() {
    _selectedDay = _today;
    _selectSlot = 'morning';
    _selectedSlotColor = Colors.green;
    _selectedTime = 1;
    _calenderController = CalendarController();
    _tabController = new TabController(initialIndex: 0, length: 3, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var _screenSize = MediaQuery.of(context).size;

    Container _backButton = new Container(
        margin: const EdgeInsets.only(left: 15, top: 10),
        alignment: Alignment.topLeft,
        child: new GestureDetector(
          child: new Icon(
            Icons.arrow_back_ios,
            color: Colors.white,
          ),
          onTap: () {
            Navigator.pop(context);
          },
        ));
    Widget _hours(int date) {
      return new Container(
          alignment: Alignment.center,
          child: new DraggableScrollableSheet(
              initialChildSize: 0.88,
              minChildSize: 0.88,
              maxChildSize: 0.99,
              builder: (context, _resScrollController) => Container(
                      child: new SingleChildScrollView(
                    controller: _resScrollController,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        new Container(
                          height: 30,
                          child: new Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              new SizedBox(
                                width: 1,
                              ),
                              new GestureDetector(
                                child: new Container(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  alignment: Alignment.center,
                                  decoration: _selectedTime == 1
                                      ? _selectedDec
                                      : _unSelectedDec,
                                  child: new Text(
                                    _selectSlot == 'morning'
                                        ? _morningHours[0]
                                        : _selectSlot == 'afternoon'
                                            ? _afternoonHours[0]
                                            : _selectSlot == 'evening'
                                                ? _eveningHours[0]
                                                : '',
                                    style: _selectedTime == 1
                                        ? _selectedStyle
                                        : _ableUnselectedStyle,
                                  ),
                                ),
                                onTap: () {
                                  setState(() {
                                    _selectedTime = 1;
                                    print('Time : 1');
                                  });
                                },
                              ),
                              new GestureDetector(
                                child: new Container(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  alignment: Alignment.center,
                                  decoration: _selectedTime == 2
                                      ? _selectedDec
                                      : _unSelectedDec,
                                  child: new Text(
                                    _selectSlot == 'morning'
                                        ? _morningHours[1]
                                        : _selectSlot == 'afternoon'
                                            ? _afternoonHours[1]
                                            : _selectSlot == 'evening'
                                                ? _eveningHours[1]
                                                : '',
                                    style: _selectedTime == 2
                                        ? _selectedStyle
                                        : _ableUnselectedStyle,
                                  ),
                                ),
                                onTap: () {
                                  setState(() {
                                    _selectedTime = 2;
                                    print('Time : 2');
                                  });
                                },
                              ),
                              new GestureDetector(
                                child: new Container(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  alignment: Alignment.center,
                                  decoration: _selectedTime == 3
                                      ? _selectedDec
                                      : _unSelectedDec,
                                  child: new Text(
                                    _selectSlot == 'morning'
                                        ? _morningHours[2]
                                        : _selectSlot == 'afternoon'
                                            ? _afternoonHours[2]
                                            : _selectSlot == 'evening'
                                                ? _eveningHours[2]
                                                : '',
                                    style: _selectedTime == 3
                                        ? _selectedStyle
                                        : _unselectedStyle,
                                  ),
                                ),

                              ),
                              new SizedBox(
                                width: 1,
                              ),
                            ],
                          ),
                        ),
                        new SizedBox(
                          height: 15,
                        ),
                        new Container(
                          height: 30,
                          child: new Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              new SizedBox(
                                width: 1,
                              ),
                              new GestureDetector(
                                child: new Container(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  alignment: Alignment.center,
                                  decoration: _selectedTime == 4
                                      ? _selectedDec
                                      : _unSelectedDec,
                                  child: new Text(
                                    _selectSlot == 'morning'
                                        ? _morningHours[3]
                                        : _selectSlot == 'afternoon'
                                            ? _afternoonHours[3]
                                            : _selectSlot == 'evening'
                                                ? _eveningHours[3]
                                                : '',
                                    style: _selectedTime == 4
                                        ? _selectedStyle
                                        : _unselectedStyle,
                                  ),
                                ),

                              ),
                              new GestureDetector(
                                child: new Container(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  alignment: Alignment.center,
                                  decoration: _selectedTime == 5
                                      ? _selectedDec
                                      : _unSelectedDec,
                                  child: new Text(
                                    _selectSlot == 'morning'
                                        ? _morningHours[4]
                                        : _selectSlot == 'afternoon'
                                            ? _afternoonHours[4]
                                            : _selectSlot == 'evening'
                                                ? _eveningHours[4]
                                                : '',
                                    style: _selectedTime == 5
                                        ? _selectedStyle
                                        : _ableUnselectedStyle,
                                  ),
                                ),
                                onTap: () {
                                  setState(() {
                                    _selectedTime = 5;
                                    print('Time : 5');
                                  });
                                },
                              ),
                              new GestureDetector(
                                child: new Container(
                                  padding: EdgeInsets.symmetric(horizontal: 10),
                                  alignment: Alignment.center,
                                  decoration: _selectedTime == 6
                                      ? _selectedDec
                                      : _unSelectedDec,
                                  child: new Text(
                                    _selectSlot == 'morning'
                                        ? _morningHours[5]
                                        : _selectSlot == 'afternoon'
                                            ? _afternoonHours[5]
                                            : _selectSlot == 'evening'
                                                ? _eveningHours[5]
                                                : '',
                                    style: _selectedTime == 6
                                        ? _selectedStyle
                                        : _ableUnselectedStyle,
                                  ),
                                ),
                                onTap: () {
                                  setState(() {
                                    _selectedTime = 6;
                                    print('Time : 6');
                                  });
                                },
                              ),
                              new SizedBox(
                                width: 1,
                              ),
                            ],
                          ),
                        ),
                        new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            new GestureDetector(
                              child: new Container(
                                  margin: const EdgeInsets.only(top: 30),
                                  width: _screenSize.width * 0.7,
                                  height: 55,
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Color(0xff0148fa),
                                      borderRadius: new BorderRadius.all(
                                          const Radius.circular(30))),
                                  child: new Text(
                                    "Confirm Booking",
                                    style: new TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w400,
                                        letterSpacing: 0.8),
                                  )),
                              onTap: () {
                                Navigator.pop(context, '$date,$_selectSlot,$_selectedTime');
                              },
                            ),
                            new SizedBox(
                              height: 20,
                            )
                          ],
                        ),
                        new SizedBox(
                          height: 20,
                        )
                      ],
                    ),
                  ))));
    }

    Column _timeOfDay = new Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        new Container(
          child: new Text(
            'Select Time Slot',
            style: TextStyle(
                fontSize: 22, fontWeight: FontWeight.w600, color: Colors.black),
          ),
          alignment: Alignment.topLeft,
          padding: const EdgeInsets.only(left: 20, top: 20),
        ),
        new Container(
          padding: const EdgeInsets.only(top: 10),
          margin: const EdgeInsets.only(bottom: 20),
          height: 45,
          child: new TabBar(
            labelPadding: EdgeInsets.symmetric(horizontal: 5),
            isScrollable: true,
            indicator: BoxDecoration(
              color: _selectedSlotColor
              ,
              borderRadius: new BorderRadius.all(const Radius.circular(30)),
            ),
            indicatorSize: TabBarIndicatorSize.label,
            controller: _tabController,
            unselectedLabelColor: Colors.grey,
            unselectedLabelStyle: TextStyle(
                fontSize: 14, fontWeight: FontWeight.w500, color: Colors.grey),
            labelStyle: TextStyle(
                fontWeight: FontWeight.w800, fontSize: 14, color: Colors.white),
            labelColor: Colors.white,
            tabs: <Widget>[
              new Tab(
                child: new GestureDetector(
                  child: new Container(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      border: Border.all(width: 1, color: Colors.grey),
                      borderRadius:
                          new BorderRadius.all(const Radius.circular(30)),
                    ),
                    child: new Row(
                      children: [
                        new Icon(
                          Icons.tag_faces,
                          size: 20,
                        ),
                        new SizedBox(
                          width: 5,
                        ),
                        new Text('Morning'),
                      ],
                    ),
                  ),
                  onTap: () {
                    _tabController.index = 0;
                    _selectSlot = 'morning';
                    setState(() {
                      _selectedSlotColor = Colors.green;
                    });
                    print('Slot : $_selectSlot');
                  },
                ),
              ),
              new Tab(
                child: new GestureDetector(
                  child: new Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Colors.grey),
                        borderRadius:
                            new BorderRadius.all(const Radius.circular(30)),
                      ),
                      child: new Row(
                        children: [
                          new Icon(
                            Icons.wb_sunny,
                            size: 20,
                          ),
                          new SizedBox(
                            width: 5,
                          ),
                          new Text('Afternoon'),
                        ],
                      )),
                  onTap: () {
                    _tabController.index = 1;
                    _selectSlot = 'afternoon';
                    setState(() {
                      _selectedSlotColor = Colors.deepOrangeAccent;
                    });
                    print('Slot : $_selectSlot');
                  },
                ),
              ),
              new Tab(
                child: new GestureDetector(
                  child: new Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Colors.grey),
                        borderRadius:
                            new BorderRadius.all(const Radius.circular(30)),
                      ),
                      child: new Row(
                        children: [
                          new Icon(
                            Icons.airline_seat_flat,
                            size: 20,
                          ),
                          new SizedBox(
                            width: 5,
                          ),
                          new Text('Evening'),
                        ],
                      )),
                  onTap: () {
                    _tabController.index = 2;
                    _selectSlot = 'evening';
                    setState(() {
                      _selectedSlotColor = Colors.black54;
                    });

                    print('Slot : $_selectSlot');
                  },
                ),
              ),
            ],
          ),
        ),
        new Container(
          height: _screenSize.height * 0.4,
          child: new TabBarView(
            controller: _tabController,
            children: <Widget>[
              new Container(child: _hours(_selectedDay.weekday)),
              new Container(child: _hours(_selectedDay.weekday)),
              new Container(child: _hours(_selectedDay.weekday)),
            ],
          ),
        )
      ],
    );
    Container _information = new Container(
      child: new DraggableScrollableSheet(
          initialChildSize: 0.78,
          minChildSize: 0.78,
          maxChildSize: 0.78,
          builder: (context, _infoScrollController) => Container(
                  child: new Container(
                decoration: BoxDecoration(
                  color: Colors.transparent,
                ),
                child: new Stack(
                  children: [
                    new Positioned(
                      top: _screenSize.height * 0.23,
                      child: new Container(
                        width: _screenSize.width,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(50)),
                        child: new SingleChildScrollView(
                          controller: _infoScrollController,
                          child: _timeOfDay,
                        ),
                      ),
                    ),
                  ],
                ),
              ))),
    );
    Positioned _homeAppBar = new Positioned(
        child: new Stack(alignment: Alignment.topCenter, children: [
      new Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          new Container(
              alignment: Alignment.topCenter,
              decoration: new BoxDecoration(
                  image: new DecorationImage(
                fit: BoxFit.fill,
                image: new AssetImage('assets/images/cov.png'),
              )),
              padding: const EdgeInsets.only(left: 20, right: 20),
              height: _screenSize.height * 0.43,
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  new SizedBox(
                    height: 50,
                  ),
                  new Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      new Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          new Icon(
                            Icons.location_on,
                            color: Colors.white,
                          ),
                          new SizedBox(
                            width: 2,
                          ),
                          new Text(
                            '${_locationPart1[0]}',
                            style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w700,
                                color: Colors.white,
                                letterSpacing: 0.8),
                          ),
                          new Text(
                            '  ${_locationPart2[0]}',
                            style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w500,
                                color: Colors.white,
                                letterSpacing: 0.8),
                          ),
                        ],
                      ),
                      new Text(
                        'Change',
                        style: TextStyle(
                          fontSize: 12,
                          fontWeight: FontWeight.w400,
                          color: Colors.white,
                        ),
                      )
                    ],
                  ),
                  new SizedBox(
                    height: 15,
                  ),
                  new Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      new Text(
                        'Select',
                        style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.w300,
                            color: Colors.white,
                            letterSpacing: 0.8),
                      ),
                      new Text(
                        ' Date',
                        style: TextStyle(
                            fontSize: 22,
                            fontWeight: FontWeight.w700,
                            color: Colors.white,
                            letterSpacing: 0.8),
                      ),
                    ],
                  ),
                  new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      TableCalendar(
                        availableCalendarFormats : const {
                          CalendarFormat.week: 'Week',
                        },
                        daysOfWeekStyle: const DaysOfWeekStyle(
                          weekdayStyle: TextStyle(color: Colors.white),
                          weekendStyle: TextStyle(color: Colors.white),
                        ),
                        initialCalendarFormat: CalendarFormat.week,
                        calendarStyle: CalendarStyle(
                            weekendStyle: (TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500)),
                            weekdayStyle: (TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w500)),
                            todayColor: Colors.white,
                            todayStyle: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18.0,
                                color: Colors.white)),
                        headerStyle: HeaderStyle(
                          leftChevronIcon: const Icon(Icons.chevron_left,
                              color: Colors.white),
                          rightChevronIcon: const Icon(Icons.chevron_right,
                              color: Colors.white),
                          titleTextStyle:
                              TextStyle(color: Colors.white, fontSize: 18),
                          centerHeaderTitle: true,
                          formatButtonVisible: false,
                        ),
                        startingDayOfWeek: StartingDayOfWeek.monday,
                        onDaySelected: (date, events) {
                          print(date.toIso8601String());
                          setState(() {
                            _selectedDay = date;
                          });
                          print('Day in Week ${date.weekday}');
                        },
                        builders: CalendarBuilders(
                          selectedDayBuilder: (context, date, events) =>
                              Container(
                                  margin: const EdgeInsets.all(4.0),
                                  alignment: Alignment.center,
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      borderRadius: BorderRadius.vertical(
                                          bottom: Radius.circular(10))),
                                  child: Text(
                                    date.day.toString(),
                                    style: TextStyle(
                                        color: Color(0xff0148fa),
                                        fontSize: 16,
                                        fontWeight: FontWeight.w600),
                                  )),
                          todayDayBuilder: (context, date, events) => Container(
                              margin: const EdgeInsets.all(4.0),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  color: Colors.white38,
                                  borderRadius: BorderRadius.circular(20.0)),
                              child: Text(
                                date.day.toString(),
                                style: TextStyle(color: Colors.white),
                              )),
                        ),
                        calendarController: _calenderController,
                      )
                    ],
                  ),
                ],
              )),
          _backButton
        ],
      ),
    ]));
    return new Scaffold(
      appBar: new AppBar(
        automaticallyImplyLeading: false,
        toolbarHeight: 0,
      ),
      body: new Container(
        height: _screenSize.height,
        alignment: Alignment.topCenter,
        child: new Stack(
          alignment: Alignment.topCenter,
          children: [
            _information,
            _homeAppBar,
            ],
        ),
      ),
    );
  }

}
