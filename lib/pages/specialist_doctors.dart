import 'package:doctor_appointment/models/doctor.dart';
import 'package:doctor_appointment/pages/doctorScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class SpecialistDoctors extends StatelessWidget {
  final String special;
  final String keySearch;
  final String date;
  List<Doctor> _cardiologyDoctors = new List();
  List<Doctor> _psycologyDoctors = new List();
  List<Doctor> _gastrologyDoctors = new List();
  List<Doctor> _neurologyDoctors = new List();
  List<Doctor> _allDoctors = new List();

  SpecialistDoctors({@required this.special, this.keySearch, this.date});

  Map<int, bool> convertWorkingDay(Map<String, bool> _stringWorkingDay) {
  return  <int, bool>{
    7: _stringWorkingDay['saturday'],
    1: _stringWorkingDay['sunday'],
    2: _stringWorkingDay['monday'],
    3: _stringWorkingDay['tuesday'],
    4: _stringWorkingDay['wednesday'],
    5: _stringWorkingDay['thursday'],
    6: _stringWorkingDay['friday'],
  };


}

  addPsycologyDoctor() {
    _psycologyDoctors = new List();
    _psycologyDoctors.add(new Doctor(
        firstName: 'Jane',
        lastName: 'Doe',
        phoneNumber: '+142865464',
        email: 'janedoe1975@gmail.com',
        reviewsRate: 135,
        rate: 3.2,
        price: 2500000,
        special: 'Psycology',
        degree: 'DM-Psycology  -  MBBS  -  MD-Psycology',
        experience: 4,
        image: new Image(
            image: new AssetImage('assets/images/doctors/woman1.jpg')),
        cover: new Image(
            image: new AssetImage('assets/images/doctors/cover_woman1.jpg')),
        avatar: new Image(
            image: new AssetImage('assets/images/doctors/avatar_woman1.png')),
        workingTimeStart: 1000,
        workingTimeEnd: 1800,
        workingDay: <String, bool>{
          'saturday': false,
          'sunday': false,
          'monday': false,
          'tuesday': true,
          'wednesday': true,
          'thursday': true,
          'friday': true,
        }));
    _psycologyDoctors.add(new Doctor(
        firstName: 'John',
        lastName: 'Doe',
        phoneNumber: '+142445630',
        email: 'johndoe1968@gmail.com',
        reviewsRate: 1024,
        price: 10000000,
        rate: 4.8,
        special: 'Psycology',
        degree: 'DM-Psycology  -  MBBS  -  MD-Psycology',
        experience: 10,
        image:
            new Image(image: new AssetImage('assets/images/doctors/man1.jpg')),
        cover: new Image(
            image: new AssetImage('assets/images/doctors/cover_man1.jpg')),
        avatar: new Image(
            image: new AssetImage('assets/images/doctors/avatar_man1.png')),
        workingTimeStart: 0800,
        workingTimeEnd: 1500,
        workingDay: <String, bool>{
          'saturday': false,
          'sunday': false,
          'monday': true,
          'tuesday': true,
          'wednesday': false,
          'thursday': true,
          'friday': false,
        }));
    _psycologyDoctors.add(new Doctor(
        firstName: 'Lilly',
        lastName: 'Smith',
        phoneNumber: '+1424894556',
        email: 'lillysmith1975@gmail.com',
        reviewsRate: 853,
        price: 1500000,
        rate: 4.0,
        special: 'Psycology',
        degree: 'DM-Psycology  -  MBBS  -  MD-Psycology',
        experience: 12,
        image: new Image(
            image: new AssetImage('assets/images/doctors/woman2.jpg')),
        cover: new Image(
            image: new AssetImage('assets/images/doctors/cover_woman2.jpg')),
        avatar: new Image(
            image: new AssetImage('assets/images/doctors/avatar_woman2.png')),
        workingTimeStart: 2000,
        workingTimeEnd: 0700,
        workingDay: <String, bool>{
          'saturday': false,
          'sunday': true,
          'monday': true,
          'tuesday': false,
          'wednesday': true,
          'thursday': false,
          'friday': true,
        }));
  }

  addCardiologyDoctor() {
    _cardiologyDoctors = new List();
    _cardiologyDoctors.add(new Doctor(
        firstName: 'Sunil',
        lastName: 'Roy',
        phoneNumber: '+142840019',
        email: 'sunilroy1968@gmail.com',
        reviewsRate: 734,
        rate: 4.9,
        price: 8750000,
        special: 'Cardiology MHW',
        degree: 'DM-Cardiology  -  MBBS  -  MD-Cardiology',
        experience: 16,
        image:
            new Image(image: new AssetImage('assets/images/doctors/man2.jpg')),
        cover: new Image(
            image: new AssetImage('assets/images/doctors/cover_man2.jpg')),
        avatar: new Image(
            image: new AssetImage('assets/images/doctors/avatar_man2.png')),
        workingTimeStart: 0800,
        workingTimeEnd: 1500,
        workingDay: <String, bool>{
    'saturday': false,
    'sunday': true,
    'monday': true,
    'tuesday': true,
    'wednesday': true,
    'thursday': false,
    'friday': true
        }));
    _cardiologyDoctors.add(new Doctor(
        firstName: 'Liv',
        lastName: 'Tyler',
        phoneNumber: '+142778319',
        email: 'livtylor1989@gmail.com',
        reviewsRate: 87,
        price: 50000,
        rate: 2.8,
        special: 'Cardiology MHW',
        degree: 'DM-Cardiology  -  MBBS  -  MD-Cardiology',
        experience: 2,
        image: new Image(
            image: new AssetImage('assets/images/doctors/woman3.jpg')),
        cover: new Image(
            image: new AssetImage('assets/images/doctors/cover_woman3.jpg')),
        avatar: new Image(
            image: new AssetImage('assets/images/doctors/avatar_woman3.png')),
        workingTimeStart: 1400,
        workingTimeEnd: 2000,
        workingDay: <String, bool>{
          'saturday': false,
          'sunday': false,
          'monday': true,
          'tuesday': true,
          'wednesday': false,
          'thursday': true,
          'friday': false,
        }));
  }

  addGastrologyDoctor() {
    _gastrologyDoctors = new List();
    _gastrologyDoctors.add(new Doctor(
        firstName: 'Jack',
        lastName: 'Sheperd',
        phoneNumber: '+14245475',
        email: 'jacksheperd1948@gmail.com',
        reviewsRate: 12,
        rate: 1.5,
        price: 150000,
        special: 'Gastrology',
        degree: 'DM-Gastrology  -  MBBS  -  MD-Gastrology',
        experience: 1,
        image:
            new Image(image: new AssetImage('assets/images/doctors/man3.jpg')),
        cover: new Image(
            image: new AssetImage('assets/images/doctors/cover_man3.jpg')),
        avatar: new Image(
            image: new AssetImage('assets/images/doctors/avatar_man3.png')),
        workingTimeStart: 0800,
        workingTimeEnd: 2000,
        workingDay: <String, bool>{
          'saturday': false,
          'sunday': true,
          'monday': false,
          'tuesday': true,
          'wednesday': true,
          'thursday': false,
          'friday': true,
        }));
  }

  addNeurologyDoctor() {
    _neurologyDoctors = new List();
  }
  addAllDoctor() {
    addPsycologyDoctor();
    addCardiologyDoctor();
    addGastrologyDoctor();
    addNeurologyDoctor();
    for (var i = 0; i < _psycologyDoctors.length; i++) {
      _allDoctors.add(_psycologyDoctors[i]);
    }
    for (var i = 0; i < _cardiologyDoctors.length; i++) {
      _allDoctors.add(_cardiologyDoctors[i]);
    }
    for (var i = 0; i < _gastrologyDoctors.length; i++) {
      _allDoctors.add(_gastrologyDoctors[i]);
    }
    for (var i = 0; i < _neurologyDoctors.length; i++) {
      _allDoctors.add(_neurologyDoctors[i]);
    }
  }

  List<Doctor> getPsycologyDoctor() {
    addPsycologyDoctor();
    return _psycologyDoctors;
  }

  List<Doctor> getCardiologyDoctor() {
    addCardiologyDoctor();
    return _cardiologyDoctors;
  }

  List<Doctor> getGastrologyDoctor() {
    addGastrologyDoctor();
    return _gastrologyDoctors;
  }

  List<Doctor> getNeurologyDoctor() {
    addNeurologyDoctor();
    return _neurologyDoctors;
  }

  List<Doctor> getAllDoctor() {
    addAllDoctor();
    return _allDoctors;
  }

  List<Doctor> filterTime(List<Doctor> _doctorList, String date) {
    final List<Doctor> _resultSearch = new List();
    if (date.substring(2) == '----') {
      return _doctorList;
    } else {
      int _searchTimeStart = int.parse(date.substring(2));
      int _searchTimeEnd = int.parse(date.substring(2)) + 30;
      if (int.parse(_searchTimeEnd.toString().substring(_searchTimeEnd
          .toString()
          .length - 2)) >= 60) {
        _searchTimeEnd = int.parse(date.substring(2)) + 70;
      }
        print('Search Time: $_searchTimeStart - $_searchTimeEnd');
        for (var i = 0; i < _doctorList.length; i++) {
          print('Doctors Time $i : ${_doctorList[i]
              .workingTimeStart} ${_doctorList[i].workingTimeEnd}');
          if (_doctorList[i].workingTimeStart < _searchTimeStart
              && _doctorList[i].workingTimeEnd > _searchTimeEnd) {
            _resultSearch.add(_doctorList[i]);
          }
        }
        return _resultSearch;
      }

    }
  List<Doctor> filterDay(List<Doctor> _doctorList, String date) {
    final List<Doctor> _resultSearch = new List();
    int day = int.parse(date.substring(0,1));
    for(var i=0 ; i < _doctorList.length ; i++) {
      print('Doctors $i, DayOfWeek : ${convertWorkingDay(_doctorList[i].workingDay)[day]}');
      if(convertWorkingDay(_doctorList[i].workingDay)[day]) {
        _resultSearch.add(_doctorList[i]);
      }
    }
    return filterTime(_resultSearch, date);
  }

  List<Doctor> getResultsSearch(String keySearch, String date) {
    if(keySearch == '' || keySearch == null) {
      addAllDoctor();
      return filterDay(_allDoctors, date);
    }
    RegExp exp = new RegExp(r'(\w*'+ keySearch +r'*\w)');

    if(exp.hasMatch('psycology')) {
      addPsycologyDoctor();
      return filterTime(_psycologyDoctors, date);
    }
    if(exp.hasMatch('cardiology')) {
      addCardiologyDoctor();
      return filterTime(_cardiologyDoctors, date);
    }
    if(exp.hasMatch('gastrology')) {
      addGastrologyDoctor();
      return filterTime(_gastrologyDoctors, date);
    }
    if(exp.hasMatch('neurology')) {
      addNeurologyDoctor();
      return filterTime(_neurologyDoctors, date);

    }
    return null;
  }

  Widget _myListViewBuilder(List<Doctor> _doctorsList) {
    return ListView.builder(
        itemCount: _doctorsList.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return new Row(children: [
            new SizedBox(
              width: 15,
            ),
            new GestureDetector(
              onTap: () {
                Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) =>
                            new DoctorScreen(doctor: _doctorsList[index])));
              },
              child: Container(
                  margin: const EdgeInsets.only(bottom: 10, top: 10),
                  decoration: BoxDecoration(
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.grey[600],
                            blurRadius: 4,
                            offset: Offset.fromDirection(-10)),
                      ],
                      color: Color(0xfff3f6ff),
                      borderRadius: BorderRadius.circular(15)),
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      new Container(
                        alignment: Alignment.topCenter,
                        height: 140,
                        width: 140,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            image: DecorationImage(
                              image: _doctorsList[index].image.image,
                            )),
                      ),
                      new SizedBox(
                        height: 5,
                      ),
                      new Container(
                        margin: const EdgeInsets.only(
                            left: 10, right: 10, bottom: 10),
                        child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              new Text(
                                'Dr. ${_doctorsList[index].firstName} ${_doctorsList[index].lastName}',
                                style: TextStyle(
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                              new Text(
                                _doctorsList[index].special,
                                style: TextStyle(
                                    fontSize: 12,
                                    fontWeight: FontWeight.w500,
                                    color: Colors.grey),
                              ),
                              new SizedBox(
                                height: 3,
                              ),
                              new Row(
                                children: [
                                  new SmoothStarRating(
                                      allowHalfRating: false,
                                      onRated: (v) {},
                                      starCount: 5,
                                      rating: _doctorsList[index].rate,
                                      size: 15.0,
                                      isReadOnly: true,
                                      filledIconData: Icons.star,
                                      halfFilledIconData: Icons.star_half,
                                      color: Colors.amber,
                                      borderColor: Colors.amber,
                                      spacing: 0.0),
                                  new SizedBox(
                                    width: 5,
                                  ),
                                  new Text(
                                    _doctorsList[index].rate.toString(),
                                    style: TextStyle(color: Colors.amber),
                                  ),
                                  new SizedBox(
                                    width: 10,
                                  ),
                                  new Icon(
                                    Icons.arrow_forward,
                                    color: Colors.grey,
                                    size: 15,
                                  ),
                                ],
                              ),
                            ]),
                      )
                    ],
                  )),
            ),
          ]);
        });
  }

  Widget _resultSearchBuilder(List<Doctor> _doctorsList) {
    if(_doctorsList == null) {
        return new Container(
          alignment: Alignment.center,
          child: new Text('not Existing any Doctor on this Time !'),
        );
      } else {
      return ListView.builder(
          itemCount: _doctorsList.length,
          scrollDirection: Axis.vertical,
          itemBuilder: (context, index) {
            return new Column(children: [
              new SizedBox(
                height: 20,
              ),
              new GestureDetector(
                onTap: () {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) =>
                          new DoctorScreen(doctor: _doctorsList[index])));
                },
                child: Container(
                    child: new Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        new Container(
                          width: 50,
                          height: 50,
                          child: _doctorsList[index].avatar != null
                              ? new CircleAvatar(
                            child: _doctorsList[index].avatar,
                            backgroundColor: Color(0xffecf0fc),
                          )
                              : new CircleAvatar(
                            backgroundColor: Colors.grey,
                          ),
                        ),
                        new SizedBox(
                          width: 10,
                        ),
                        new Container(
                          padding:
                          const EdgeInsets.symmetric(
                              horizontal: 13, vertical: 7),
                          width: 240,
                          height: 85,
                          decoration: BoxDecoration(
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                    color: Colors.grey[600],
                                    blurRadius: 4,
                                    offset: Offset.fromDirection(-10)),
                              ],
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(15)),
                          child: new Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              new Text(
                                'Dr. ${_doctorsList[index]
                                    .firstName} ${_doctorsList[index]
                                    .lastName}',
                                style: TextStyle(
                                    fontSize: 16, fontWeight: FontWeight.w600),
                              ),
                              new SizedBox(
                                height: 5,
                              ),
                              new Row(
                                children: [
                                  new Container(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 7, vertical: 3),
                                    decoration: BoxDecoration(
                                      color: Color(0xffecf0fc),
                                      borderRadius: BorderRadius.circular(15),
                                    ),
                                    child: new Text(_doctorsList[index].special,
                                        style: TextStyle(
                                            fontSize: 12,
                                            fontWeight: FontWeight.w500,
                                            color: Colors.black,
                                            letterSpacing: 0.6)),
                                  ),
                                  new SizedBox(
                                    width: 15,
                                  ),
                                  new Text(
                                    '${_doctorsList[index].experience}+ Years',
                                    style: TextStyle(
                                        fontWeight: FontWeight.w500),
                                  ),
                                ],
                              ),
                              new SizedBox(
                                height: 5,
                              ),
                              new Text(
                                  _doctorsList[index].degree,
                                  style: TextStyle(
                                      fontSize: 10,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.grey[600])),
                            ],
                          ),
                        ),
                      ],
                    )),
              ),
            ]);
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    switch (this.special) {
      case 'psycology':
        {
          addPsycologyDoctor();
          return _myListViewBuilder(_psycologyDoctors);
        }
      case 'cardiology':
        {
          addCardiologyDoctor();
          return _myListViewBuilder(_cardiologyDoctors);
        }
      case 'gastrology':
        {
          addGastrologyDoctor();
          return _myListViewBuilder(_gastrologyDoctors);
        }
      case 'neurology':
        {
          addNeurologyDoctor();
          return _myListViewBuilder(_neurologyDoctors);
        }
      case 'all':
        {
          addAllDoctor();
          return _myListViewBuilder(_allDoctors);
        }
      case 'search':
        {
          return
              _resultSearchBuilder(getResultsSearch(
                        keySearch != null ? keySearch : '',
                        date != null ? date : DateTime.now().weekday.toString()));
        }
    }
    return null;
  }
}
