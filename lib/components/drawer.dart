

import 'package:flutter/material.dart';


Drawer buildDrawerLayout(BuildContext context) {
  return new Drawer(
    child: new ListView(
      children: <Widget>[
        new DrawerHeader(

          padding: EdgeInsets.zero,
            child: new Container(
              decoration: new BoxDecoration(
                  gradient: new LinearGradient(
                    colors: <Color>[
                      const Color(0xff0148fa),
                      const Color(0xff0148fa),
                      const Color(0xff5189f9),
                      const Color(0xff5189f9),
                      const Color(0xff0148fa),
                    ],
                  begin: Alignment.topLeft,
                  end: Alignment.bottomRight
                )
              ),
              child: new Stack(
                children: <Widget>[
                  new Align(
                    alignment: Alignment.bottomLeft,
                    child: new Padding(
                        padding: const EdgeInsets.only(left: 50, bottom: 15),
                      child: new Text(
                          "HiDoc.",
                          style: TextStyle(
                              fontSize: 22,
                              color: Colors.white,
                              fontWeight: FontWeight.w600
                          ),
                    )
                    ),
                  ),
                  new Align(
                    alignment: Alignment.topRight,
                    child: new Padding(
                        padding: const EdgeInsets.only(left: 5, top: 15),
                      child:  ListTile(
                      leading: new CircleAvatar(
                        backgroundColor: Colors.grey,
                      ),
                      title: new Text(
                        'My Name',
                        style: TextStyle(
                            fontSize: 15,
                            color: Colors.white,
                            fontWeight: FontWeight.w400
                        ),
                    ),
                  ),
                    )
                  )
                ],
              ),
            ),
        ),
        new ListTile(
          leading: new Icon(Icons.call),
          title: new Text("Emergency Call...",
            style: TextStyle(
                fontFamily: 'Nazanin',
                fontWeight: FontWeight.w600,
                fontSize: 16
            ),
          ),
          onTap: (){},
        ),
        new ListTile(
          leading: new Icon(Icons.person),
          title: new Text("Profile",
            style: TextStyle(
                fontFamily: 'Nazanin',
                fontWeight: FontWeight.w600,
                fontSize: 16
            ),
          ),
          onTap: (){},
        ),
        new ListTile(
          leading: new Icon(Icons.inbox),
          title: new Text("Inbox",
            style: TextStyle(
                fontFamily: 'Nazanin',
                fontWeight: FontWeight.w600,
                fontSize: 16
            ),
          ),
          onTap: () {

          },
        ),
        new ListTile(
          leading: new Icon(Icons.calendar_today),
          title: new Text("Calendar",
            style: TextStyle(
                fontFamily: 'Nazanin',
                fontWeight: FontWeight.w600,
                fontSize: 16
            ),
          ),
          onTap: () {

          },
        ),
        new ListTile(
          leading: new Icon(Icons.history),
          title: new Text("History",
            style: TextStyle(
                fontFamily: 'Nazanin',
                fontWeight: FontWeight.w600,
                fontSize: 16
            ),
          ),
          onTap: () {

          },
        ),
        new ListTile(
          leading: new Icon(Icons.info),
          title: new Text("App Info",
            style: TextStyle(
                fontFamily: 'Nazanin',
                fontWeight: FontWeight.w600,
                fontSize: 16
            ),
          ),
          onTap: (){
//            Navigator.push(context, new MaterialPageRoute(builder: (context) => ContactUs()));
          },
        ),

        new ListTile(
          leading: new Icon(Icons.exit_to_app),
          title: new Text("Log Out",
            style: TextStyle(
                fontFamily: 'Nazanin',
                fontWeight: FontWeight.w600,
                fontSize: 16
            ),
          ),
          onTap: (){
//            Navigator.push(context, new MaterialPageRoute(builder: (context) => ContactUs()));
          },
        ),

      ],
    ),
  );
}
